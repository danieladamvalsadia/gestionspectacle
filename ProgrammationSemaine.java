package TheProject2017;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;


import java.util.Map.Entry;




public class ProgrammationSemaine {

	int semaine;
	ArrayList<Salle> salleCinema;
	ArrayList<SalleTheatre> salleTheatre;

	private SortedMap<SeanceCinema, Film> ensFilm;
	private SortedMap<SeanceTheatre, PieceTheatre> ensPieceTheatre;


	public ProgrammationSemaine(int semaine) { 
		this.semaine = semaine;
		ensFilm = new TreeMap<SeanceCinema, Film>();
		ensPieceTheatre = new TreeMap<SeanceTheatre, PieceTheatre>();


	}

// ADDSEANCES
	
	/*
	 * Methode addSeances 
	 * 
	 * 
	 */
	public void addSeances(Set<SeanceCinema> seancesCinema, Film film) {

		for(SeanceCinema x : seancesCinema)
			if(canAdd(x,film)) {
				ensFilm.put(x ,film);

			}else System.out.println("Seance deja en cours");

		
	}
	
	/*
	 * Methode addSeances
	 */
	public void addSeances(Set<SeanceTheatre> seancesTheatre, PieceTheatre pieceTheatre) {
		
		for(SeanceTheatre x : seancesTheatre)
			if(canAdd(x,pieceTheatre)) {
				ensPieceTheatre.put(x, pieceTheatre);
			}
		}
	
// GETSEANCESCINEMA
	
	/*
	 * Methode getEnsSeancesFilm
	 */
	public  Set<SeanceCinema> getEnsSeancesFilm(Film film) {
		Set<SeanceCinema> seanceCinema  = new TreeSet<SeanceCinema>();
		for (Entry<SeanceCinema, Film> x : ensFilm.entrySet()) {
			if (x.getValue().equals(film)) {
				seanceCinema.add(x.getKey());
			}
		}
		return seanceCinema;
	}
	
	/*
	 * Methode getEnsSeancesFilm
	 */
	public  Set<SeanceCinema> getEnsSeancesFilm(Film film , int jour) {
		Set<SeanceCinema> seanceCinema  = new TreeSet<SeanceCinema>();
		for (Entry<SeanceCinema, Film> x : ensFilm.entrySet()) {
			if ((x.getValue().equals(film)) && ( x.getKey().getJour() == jour )) {
				seanceCinema.add(x.getKey());
			}
		}
		return seanceCinema;
	}

// GETSEANCESTHEATRE
	
	/*
	 * Methode getEnsSeancesTheatre
	 */
	public  Set<SeanceTheatre> getEnsSeancesTheatre(PieceTheatre p) {
		Set<SeanceTheatre> seanceTheatre  = new TreeSet<SeanceTheatre>();
		for (Entry<SeanceTheatre, PieceTheatre> x : ensPieceTheatre.entrySet()) {
			if (x.getValue().equals(p)) {
				seanceTheatre.add(x.getKey());
			}
		}
		return seanceTheatre;
	}
	
	/*
	 * Methode getEnsSeancesTheatre
	 */
	public  Set<SeanceTheatre> getEnsSeancesTheatre(PieceTheatre p, int jour) {
		Set<SeanceTheatre> seanceTheatre  = new TreeSet<SeanceTheatre>();
		for (Entry<SeanceTheatre, PieceTheatre> x : ensPieceTheatre.entrySet()) {
			if ((x.getValue().equals(p)) && ( x.getKey().getJour() == jour )) {
				seanceTheatre.add(x.getKey());
			}
		}
		return seanceTheatre;
	}

// REMOVE
	
	/*
	 * Methode retirerFilm
	 */
	public void retirerfilm(Film f) {
		Set<SeanceCinema> sC = new TreeSet<SeanceCinema>();

		for(Entry<SeanceCinema,Film> x : ensFilm.entrySet()) {
			if(x.getValue()==f)
				sC.add(x.getKey());
		}
		for(SeanceCinema  y : sC) {
			ensFilm.remove(y);
		}

	}
	
	/*
	 * Methode retirerPieceTheatre
	 */
	public void retirerPieceTheatre(PieceTheatre p) { 
		Set<SeanceTheatre> sT = new TreeSet<SeanceTheatre>();

		for(Entry<SeanceTheatre,PieceTheatre> x : ensPieceTheatre.entrySet()) {
			if(x.getValue()==p)
				sT.add(x.getKey());
		}
		for(SeanceTheatre  y : sT) {
			ensPieceTheatre.remove(y);
		}

	}


// RECUP
	
	/*
	 * Methode recupFilm
	 * 
	 * Cette methode recupere tout les films d'une semaine.
	 */
	public Set<Film> recupFilm (){ 
		Set<Film> resu = new HashSet<Film>();

		for(Entry<SeanceCinema, Film> x : ensFilm.entrySet()) {
			resu.add(x.getValue());
		}
		return resu ;
	}
	
	/*
	 * Methode recupTheatre
	 * 
	 * Cette methode recupere toutes les pieces de theatre d'une semaine
	 */
	public Set<PieceTheatre> recupTheatre (){ 
		
		Set<PieceTheatre> resu = new HashSet<PieceTheatre>();

		for(Entry<SeanceTheatre, PieceTheatre> x : ensPieceTheatre.entrySet()) {
			resu.add(x.getValue());
		}
		return resu ;
	}

//NBSPECTACLE
	
	/*
	 * Methode nbFilm
	 * 
	 * Cette methode retourne le nombre de film du Set Film
	 */
	public int nbFilm () { 
		Set<Film> x = new HashSet<Film>() ;
		for(Entry<SeanceCinema, Film> y : ensFilm.entrySet()) {
			x.add(y.getValue());
		}
		return x.size();
	}
	
	/*
	 * Methode nbPieceTheatre
	 * 
	 * Cette methode retourne le nombre de piece de theatre du Set PieceTheatre
	 */
	public int nbPieceTheatre () { 
		Set<PieceTheatre> x = new HashSet<PieceTheatre>() ;
		for(Entry<SeanceTheatre, PieceTheatre> y : ensPieceTheatre.entrySet()) {
			x.add(y.getValue());
		}
		return x.size();
	}
	
//ESTPRESENT
	
	/*
	 * Methode estPresent
	 * 
	 * Cette methode retourne vrai si un film est programme dans une semaine sinon
	 */
	public boolean estPresent(Film f) {	 
		return this.ensFilm.containsValue(f);
	}
	
	/*
	 * Mehtode estPresent
	 * 
	 * Cette methode retourne vrai si une piece de theatre est programmee dans une semaine
	 */
	public boolean estPresent(PieceTheatre p) {	
		return this.ensPieceTheatre.containsValue(p);
	}

//CONSULTER
	
	/*
	 * Methode consulterSeance
	 */
	public SeanceCinema consulterSeance(Film f, int jour, Heure horaire) {
		SeanceCinema resu = null;
		for (Entry<SeanceCinema, Film> x : ensFilm.entrySet()) {
			if (x.getKey().getJour()==jour && x.getKey().getHoraire().equals(horaire) && x.getValue().equals(f))
				resu = (x.getKey());		
		}
		return resu;
	}
	
	/*
	 * Methode consulterSeance
	 */
	public SeanceTheatre consulterSeance(PieceTheatre p, int jour, Heure horaire) {
		SeanceTheatre resu = null;
		for (Entry<SeanceTheatre, PieceTheatre> x : ensPieceTheatre.entrySet()) {
			if (x.getKey().getJour()==jour && x.getKey().getHoraire().equals(horaire) && x.getValue().equals(p))
				resu = (x.getKey());		
		}
		return resu;
	}

	/*
	 * Methode getEnsFilm
	 */
	public SortedMap<SeanceCinema, Film> getEnsFilm() {
		return ensFilm;
	}

	/*
	 * Methode setEnsFilm
	 */
	public void setEnsFilm(SortedMap<SeanceCinema, Film> ensFilm) {
		this.ensFilm = ensFilm;
	}

	/*
	 * Methode getEnsPieceTheatre
	 */
	public SortedMap<SeanceTheatre, PieceTheatre> getEnsPieceTheatre() {
		return ensPieceTheatre;
	}

	/*
	 * Methode setEnsPieceTheatre
	 */
	public void setEnsPieceTheatre(SortedMap<SeanceTheatre, PieceTheatre> ensPieceTheatre) {
		this.ensPieceTheatre = ensPieceTheatre;
	}

	//CANADD
	
	/*
	 * Methode canAdd
	 * 
	 * Cette methode permet de savoir si on peut ajouter une seance ou non
	 */
	public boolean canAdd(SeanceCinema seanceC, Film f) {			
		for (Entry<SeanceCinema, Film> x : ensFilm.entrySet()) {
			if (x.getKey().getSalle().getNomSalle().equals(seanceC.getSalle().getNomSalle())
					&& x.getKey().getJour() == seanceC.getJour()) {
				Heure heureFinMap = getHeureDeFin(x.getValue().getDuree(), x.getKey().getHoraire());
				Heure heureFinSeance = getHeureDeFin(f.getDuree(), seanceC.getHoraire());
				Heure heureDebMap = x.getKey().getHoraire();
				Heure heureDebSeance = seanceC.getHoraire();
				
				if (heureDebSeance.compareTo(heureDebMap) >= 0 && heureDebSeance.compareTo(heureFinMap) < 0)
					return false;
				if (heureFinSeance.compareTo(heureDebMap) > 0 && heureFinSeance.compareTo(heureFinMap) <= 0)
					return false;
				if (heureFinMap.compareTo(heureDebSeance) > 0 && heureFinMap.compareTo(heureFinSeance) <= 0)
					return false;
				if (heureDebMap.compareTo(heureDebSeance) >= 0 && heureDebMap.compareTo(heureFinSeance) < 0)
					return false;
			}
		}
		return true;

	}
	
	/*
	 * Methode canAdd
	 * 
	 * Cette methode permet de savoir si on peut ajouter une seance ou non
	 */
	public boolean canAdd(SeanceTheatre sC , PieceTheatre p) {
		for (Entry<SeanceTheatre, PieceTheatre> x : ensPieceTheatre.entrySet()) {
			if (x.getKey().getSalle().getNomSalle().equals(sC.getSalle().getNomSalle())
					&& x.getKey().getJour() == sC.getJour()) {
				return false;
			}
		}
		return true;

	}

	/*
	 * Methode getHeureDeFin
	 * 
	 */
	public Heure getHeureDeFin(Heure a , Heure b ) {
		int heure = a.getHeures() + b.getHeures();
		int minute = a.getMinutes() + b.getMinutes();
		if (minute>59) {
			minute = minute-60;
			heure = heure + 1;
		}
		return new Heure(heure,minute);
	}
	
	/*
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {

		String resu = "ProgrammationSemaine : Semaine = " + semaine + "\nEnsFilm = " + ensFilm + "\n\nEnsPieceTheatre = "
				+ ensPieceTheatre ;
		return resu;
	}

}

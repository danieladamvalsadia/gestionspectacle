package TheProject2017;

public abstract class Seance implements Comparable<Seance>{

	int jour;
	Heure horaire;
	int nbPlacesVenduesTN = 0;

	public abstract int nbPlacesDispo();

	public abstract int totalVendu();

	public abstract double tauxRemplissage();
	
	public Seance(int jour, Heure horaire) {
		this.setJour(jour);
		this.setHoraire(horaire);
	}

	public void vendrePlacesTN(int nbre) {
		this.nbPlacesVenduesTN += nbre;
	}

	public int getJour() {
		return jour;
	}

	public void setJour(int jour) {
		this.jour = jour;
	}

	public Heure getHoraire() {
		return horaire;
	}

	public void setHoraire(Heure horaire) {
		this.horaire = horaire;
	}

	

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Seance other = (Seance) obj;
		if (horaire == null) {
			if (other.horaire != null)
				return false;
		} else if (!horaire.equals(other.horaire))
			return false;
		if (jour != other.jour)
			return false;
		return true;
	}

	public int getNbPlacesVenduesTN() {
		return nbPlacesVenduesTN;
	}

	public void setNbPlacesVenduesTN(int nbPlacesVenduesTN) {
		this.nbPlacesVenduesTN = nbPlacesVenduesTN;
	}

	

	
}
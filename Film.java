package TheProject2017;

public class Film extends Spectacle  {
	String realisateur;
	Heure duree;

	public Film(String titre, String interpretes, String realisateur, Heure duree) {
		super(titre, interpretes);
		this.interpretes = interpretes;
		this.realisateur = realisateur;
		this.duree = duree;
	}

	public String getRealisateur() {
		return this.realisateur;
	}

	public void setRealisateur(String realisateur) {
		this.realisateur = realisateur;
	}

	public void setDuree(Heure duree) {
		this.duree = duree;
	}

	public Heure getDuree() {
		return this.duree;
	}

	// @Override
	public String toString() {
		return super.toString() + "\n Realisateur : " + this.realisateur + "\n Duree : " + this.duree + "\n\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((duree == null) ? 0 : duree.hashCode());
		result = prime * result + ((realisateur == null) ? 0 : realisateur.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Film other = (Film) obj;
		if (duree == null) {
			if (other.duree != null)
				return false;
		} else if (!duree.equals(other.duree))
			return false;
		if (realisateur == null) {
			if (other.realisateur != null)
				return false;
		} else if (!realisateur.equals(other.realisateur))
			return false;
		return true;
	}

	



}

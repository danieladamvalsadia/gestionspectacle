package TheProject2017;

public class SalleTheatre extends Salle {
	int nbFauteuils;
	double prixFauteuil;

	public int getNbFauteuils() {
		return nbFauteuils;
	}

	public void setNbFauteuils(int nbFauteuils) {
		this.nbFauteuils = nbFauteuils;
	}

	public double getPrixFauteuil() {
		return prixFauteuil;
	}

	public void setPrixFauteuil(double prixFauteuil) {
		this.prixFauteuil = prixFauteuil;
	}

	public SalleTheatre(String nomSalle, int capacite, double tarif, int nbFauteuils, double prixFauteuil) {
		super(nomSalle, capacite, tarif);
		this.nbFauteuils = nbFauteuils;
		this.prixFauteuil = prixFauteuil;

	}

	public String toString() {
		return "Nom de la salle : " + this.nomSalle;
	}
}
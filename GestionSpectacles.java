package TheProject2017;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;
import java.util.HashSet;

import javax.swing.JOptionPane;

public class GestionSpectacles {
	private static Scanner sc = new Scanner(System.in);
	public static double ChiffreAffaire =0;
	private static int semaineCree = 0;
	private static ArrayList<ProgrammationSemaine> lesProgrammations; 
	private static ArrayList<Salle> EnsembleSallesCinema;
	private static ArrayList<SalleTheatre> EnsembleSallesTheatre;

	/*
	 * MAIN
	 * 
	 * Le main propose de maniere interactive avec un switch a l'utilisateur de choisir les choix suivants :
	 * 		-Creer une programmation
	 * 		-Modifier une programmation 
	 * 		-Vendre des places pour une programmation
	 * 		-Consulter les infos ventes places
	 * 		-Quitter
	 */
	public static void main(String[] args) throws IOException {
		lireFichierSallesCinema("ressources/SallesCinema.csv");
		lireFichierSallesTheatres("ressources/SallesTheatre.csv");
		lesProgrammations = new ArrayList<>();

		boolean continu = true;
		String choix = "";
		while (continu) {
			
			System.out.println("Saisir le chiffre correspondant a votre requete : ");
			System.out.println("1. Creer une programmation");
			System.out.println("2. Modifier une programmation");
			System.out.println("3. Vendre des places pour une programmation");
			System.out.println("4. Consulter les infos ventes places");
			System.out.println("5. Quitter");
			int aa=0;
			Scanner inint = new Scanner(System.in);
			while(true) {
				if(inint.hasNextInt()) {
					aa=inint.nextInt();
					break;
				}
				inint.next();
				
			}
			
			
			switch (aa) {
			case 1:
				semaineCree++;
				lesProgrammations.add(creationSemaine(semaineCree));
				break;

			case 2:
				Scanner in = new Scanner(System.in);
				if(getLesProgrammations().size() != 0) {
					int semaineAM;
			        boolean semaineExistante = true;
			        System.out.print("Saisir le numero de la semaine a modifier : ");
			        while(semaineExistante){
			        		try {
			        			semaineAM = (int) Integer.valueOf(in.next());
			        			
					        	if(semaineAM>0 && semaineAM<=getLesProgrammations().size()) {					 
					                System.out.println("Le numero de la semaine saisie est : " + semaineAM);		
					                modifSemaine(semaineAM);
					                semaineExistante = false; 
					                //in.close();
			                } else {
			        				System.out.println("Le numero de semaine saisi est inexistant.");
			        				System.out.print("Saisir le numero de la semaine a modifier : ");
			        			}
					      
			        		} catch(NumberFormatException e){
			        			System.out.print("Saisir le numero de la semaine a modifier : ");
			        		}
			        }
			        		//int semaineAM = Integer.parseInt(JOptionPane.showInputDialog("Saisir le numero de la semaine a modifier "));
					
				} else {
					System.out.println("Aucune programmation existe.");
				}
				break;
		        
			case 3:
				vendrePlace();
				break;
			case 4:
				System.out.println("Chiffre d affaire = " +getChiffreAffaire() + " €");
				int taux =0;
				int nbSeances =0;
				for(ProgrammationSemaine pS : lesProgrammations) {
					for (Entry<SeanceCinema, Film> sC : pS.getEnsFilm().entrySet()) {
						nbSeances ++;
						taux += sC.getKey().tauxRemplissage();}
					for(Entry<SeanceTheatre, PieceTheatre> sT : pS.getEnsPieceTheatre().entrySet()) {
						nbSeances ++;
						taux += sT.getKey().tauxRemplissage();}
						
				}
				if(nbSeances == 0) {
					System.out.println("Le taux de remplissage ne peut etre calcule car aucune seance n'a ete cree.");
				} else {
					System.out.println("Taux de remplissage moyen : "+taux/nbSeances + " %");
				}
				break;
			case 5:
				continu = false;
				//sc.close();
				break;
				
			}

		}

	}
	
	/*
	 * Methode creationSemaine
	 * 
	 * Cette methode prend en parametre l'increment semaineCree et construit un objet semaine a partir d'un ensemble de seance associe a un Spectacle.
	 * Il est donc demander de maniere interactive a l'utilisateur de creer un spectacle et de visualiser la programmation de la semaine prealablement cree.
	 * A l'aide d'un switch les choix suivants sont proposes : 
	 * 		-Creer un nouveau film
	 * 		-Creer une bouvelle piece de theatre
	 * 		-Visualiser la semaine 
	 * 		-Fin de la programmation de la semaine
	 */
	public static ProgrammationSemaine creationSemaine(int semaineCree) {    
		ProgrammationSemaine l = new ProgrammationSemaine(semaineCree);
		boolean continu = true;

		while (continu) {

			System.out.println("1. Creer un nouveau film ");
			System.out.println("2. Creer une nouvelle piece de theatre");
			System.out.println("3. Visualiser la semaine");
			System.out.println("4. Fin de la programmation de la semaine");
			
			int aa=0;
			Scanner inint = new Scanner(System.in);
			while(true) {
				if(inint.hasNextInt()) {
					aa=inint.nextInt();
					break;
				}
				inint.next();
				
			}
			
			
			switch (aa) {
			case 1:
				Film f = creationFilm();
				l.addSeances(creationSeanceCinema(), f);
				break;
			case 2:
				PieceTheatre pT = creationTheatre();
				l.addSeances(creationSeanceTheatre(), pT);
				break;
			case 3:
				System.out.println(l);
				break;
			case 4:
				continu=false;
				//sc.close();
				break;

			}
		}
		return l;
	}
	
	/*
	 * Methode modifSemaine
	 * 
	 * Cette methode prend en parametre l'index d'une semaine et l'a modifie.
	 * Il est possoble de creer un Spectacle ou le supprimer et de visualiser l'ensemble de la semaine indexee.
	 * Un switch propose les choix suivants : 
	 * 		-Creer un nouveau film 
	 * 		-Creer une nouvelle seance de piece de theatre
	 * 		-Supprimer un film de la programmation
	 * 		-Supprimer une piece de theatre de la programmation
	 * 		-Visualiser la semaine
	 * 		-Fin de la modification de la programmation de la semaine
	 */
	public static void modifSemaine(int semaineAM) {      

		ProgrammationSemaine l = lesProgrammations.get(semaineAM-1);
		boolean continu = true;

		while (continu) {
			System.out.println("Saisir le chiffre correspondant a votre requete : ");
			System.out.println("1. Creer un nouveau film ");
			System.out.println("2. Creer une nouvelle piece de theatre");
			System.out.println("3. Supprimer un film de la programmation");
			System.out.println("4. Supprimer une piece de theatre de la programmation");
			System.out.println("5. Visualiser la semaine");
			System.out.println("6. Fin modification de la programmation de la semaine");
			int aa=0;
			Scanner inint = new Scanner(System.in);
			while(true) {
				if(inint.hasNextInt()) {
					aa=inint.nextInt();
					break;
				}
				inint.next();
				
			}
			
			switch (aa) {
			case 1:
				Film f = creationFilm();
				l.addSeances(creationSeanceCinema(), f);

				break;

			case 2:
				PieceTheatre pT = creationTheatre();
				l.addSeances(creationSeanceTheatre(), pT);
				break;
			case 3:
				
				Set<Film> x = l.recupFilm();
				if(!x.isEmpty()) {
					int nbFilm=0;
					System.out.println("Pour quel film ? \n");
					
					for(Film a : x) {
						nbFilm++;
						System.out.println("\n Film n°"+nbFilm+ " : " +a.toString());
					}
					nbFilm=0;
					Scanner in = new Scanner(System.in);
					boolean choixTitre = true;
					int choix4;
					while(choixTitre) {
						try {
							choix4 = (int) Integer.valueOf(in.next());
							if(choix4>0 && choix4<=x.size()) {	
								Film g = null;
								for(Film y : x ) {
									choix4--;
									if (choix4 == 0)
										g = y;}
								l.retirerfilm(g);
								choixTitre = false;
							}
						} catch(NumberFormatException e){
							for(Film a : x) {
								nbFilm++;
								System.out.println("\n Film n°"+nbFilm+ " : " +a.toString());
							}
							nbFilm=0;
		        			}
					}
				} else {
					System.out.println("Aucun film existant.");
				}
				//sc.close();
				break;
			case 4:
					Set<PieceTheatre> o = l.recupTheatre();
					int nbPT=0;
					if(!o.isEmpty()) {
						System.out.println("Pour quelle piece de theatre ? \n");
						for(PieceTheatre a : o) {
							nbPT++;
							System.out.println("\n Piece de theatre n°"+nbPT+ " : " +a.toString());
						}
						nbPT=0;
						Scanner in = new Scanner(System.in);
						boolean choixPT = false;
						int choix5;
						while(choixPT) {
							try {
								choix5 = (int) Integer.valueOf(in.next());
								if(choix5>0 && choix5<=o.size()) {	
									PieceTheatre pDT = null;
									for(PieceTheatre y : o ) {
										choix5--;
										if (choix5 == 0)
											pDT = y;
										}
										l.retirerPieceTheatre(pDT);
										choixPT=false;
									//sc.close();
								}
							} catch(NumberFormatException e){
								for(PieceTheatre a : o) {
									nbPT++;
									System.out.println("\n Piece de theatre n°"+nbPT+ " : " +a.toString());
								}
								nbPT=0;
			        			}
						}
					} else {
						System.out.println("Aucune piece de theatre existante");
					}
				break;
			case 5:
				System.out.println(l);
				break;
			case 6:
				continu=false;
				//sc.close();
				break;

			}

		}


	}
	
	/*
	 * Methode vendrePlace
	 * 
	 * Cette methode propose a l'utilisateur de maniere interactive de choisir de vendre des places de cinema ou des places de theatre.
	 * A partir d'un switch l'utilisateur peut choisir les choix suivants :
	 * 		-Film
	 * 		-Piece de theatre
	 */
	public static void vendrePlace() {	
	
		System.out.println("Pour quel type de spectacle souhaitez vous prendre des places ? \n 1)Film \n 2)Piece de theatre");
		int aa=0;
		Scanner inint = new Scanner(System.in);
		while(true) {
			if(inint.hasNextInt()) {
				aa=inint.nextInt();
				break;
			}
			inint.next();
			
		}
		switch (aa) {
		case 1:
			vendrePlacecinema();
			break;
		case 2:
			vendrePlaceTheatre();
			break;
		}
	}
	
	/*
	 * Methode vendrePlace
	 * 
	 * Cette methode propose de maniere interactive a l'utilisateur de vendre des places de cinema.
	 * Il doit donc selectionner une semaine, une piece de cinema, sa seance, son jour, son heure, ses minutes.
	 * Enfin l'utilisateur accede a un ensemble d'informations sur la vente de places.
	 */
	public static void vendrePlacecinema () {			
		
		
		//CHOIX SEMAINE
		System.out.println("Pour quelle semaine ? ");
		if(getLesProgrammations().size() != 0) {
			ProgrammationSemaine l = null;
			boolean semaineExistante = true;
			int choix=0;
	        System.out.print("Saisir le numero de la semaine a modifier : ");
	        while(semaineExistante){
	        		try {
	        			choix = (int) Integer.valueOf(sc.next());
	        			
			        	if(choix>0 && choix<=getLesProgrammations().size()) {					 
			                System.out.println("Le numero de la semaine saisie est : " + choix);		
			                l = lesProgrammations.get(choix-1);
			                semaineExistante = false; 
			             
	                } else {
	        				System.out.println("Le numero de semaine saisi est inexistant.");
	        				System.out.print("Saisir le numero de la semaine a modifier : ");
	        			}
			      
	        		} catch(NumberFormatException e){
	        			System.out.print("Saisir le numero de la semaine a modifier : ");
	        		}
	        }
			
			// CHOIX FILM
	        Set<Film> x = l.recupFilm();
	        if(!x.isEmpty()) {
				System.out.println("Pour quel film ? \n");
				
				int nbFilm =0;
				Film f = null;
				for(Film a : x) {
					nbFilm++;
					System.out.println("\n Film n°"+nbFilm+ " : " +a.toString());
				}
				nbFilm=0;
				Scanner in = new Scanner(System.in);
				boolean choixTitre = true;
				int choix2 =0;
				while(choixTitre) {
					try {
						choix2 = (int) Integer.valueOf(in.next());
						if(choix2>0 && choix2<=x.size()) {	
							
							for(Film y : x ) {
								choix2--;
								if (choix2 == 0)
									f = y;
							}
							Set<SeanceCinema> z = l.getEnsSeancesFilm(f);
							System.out.println(z);
							choixTitre = false;
							
							//in.close();
						}
					} catch(NumberFormatException e){
						for(Film a : x) {
							nbFilm++;
							System.out.println("\n Film n°"+nbFilm+ " : " +a.toString());
							System.out.println("Pour quel film ? \n");
						}
						nbFilm=0;
	        			}
				}
				
				
				
				
				//CHOIX JOUR SEANCE
				System.out.println("Saisir le jour de la seance : " );
				boolean choixJour = true;
				int jour2=0;
				while(choixJour) {
					try {
						jour2 = (int) Integer.valueOf(in.next());
						
						if(l.getEnsSeancesFilm(f, jour2).size() > 0) {
							choixJour = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir le jour de la seance : " );
	        			}
				}
				
				// CHOIX HEURE SEANCE
				System.out.println("Saisir l'heure de la seance : ");
				boolean choixHeure = true;
				int heure2=0;
				while(choixHeure) {
					try {
						heure2 = (int) Integer.valueOf(in.next());
						if(heure2 >= 0 && heure2 <=24) {
							choixHeure = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir l'heure de la seance : ");
					}
	    			}
				
				// CHOIX MINUTE SEANCE
				System.out.println("Saisir les minutes : ");
				//int minute2 = Integer.parseInt(JOptionPane.showInputDialog("Minute ? "));
				boolean choixMinute = true;
				int minute2=0;
				while(choixMinute) {
					try {
						minute2 = (int) Integer.valueOf(in.next());
						if(minute2>=0 && minute2 <=59) {
							choixMinute = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir les minutes : ");
					}
				}
				Heure Horaire =  new Heure(heure2 ,minute2);
				SeanceCinema sC = l.consulterSeance(f,jour2 , Horaire);
				
				
				System.out.println("Nb de places disponible : " + sC.nbPlacesDispo());
				System.out.println("Nb de places vendu pour cette seance  : " + sC.totalVendu());
				
				// CHOIX TARIF REDUIT TICKET
				System.out.println("Combien de tickets en tarif reduit ? ");
				boolean choixTR = true;
				int a = 0;
				while(choixTR) {
					try {
						a = (int) Integer.valueOf(in.next());
						if(a>0 && a<=sC.nbPlacesDispo()) {
							sC.vendrePlacesTR(a);
							choixTR = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Combien de tickets en tarif reduit ? ");
					}
				}
			
				// CHOIX TARIF NORMAL TICKET
				System.out.println("Combien de tickets en tarif normal ? ");
				boolean choixTN = true;
				int b = 0;
				while(choixTN) {
					try {
						b = (int) Integer.valueOf(in.next());
						if(b>0 && b<=sC.nbPlacesDispo()) {
							sC.vendrePlacesTN(b);
							choixTN = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Combien de tickets en tarif normal ? ");
					}
				}
			
				double tarif = (a*sC.getSalle().getTarif()*0.6 )+(b*sC.getSalle().getTarif());
				System.out.println("A regler : "+tarif +" €");
				ChiffreAffaire += tarif;
			} else {
				System.out.println("Aucune programmation existante");
			}
		} else {
			System.out.println("Aucun film existant pour cette semaine");
		}
	}
	
	/*
	 * Methode vendrePlace
	 * 
	 * Cette methode propose de maniere interactive a l'utilisateur de vendre des places de theatre.
	 * Il doit donc selectionner une semaine, une piece de theatre, sa seance, son jour, son heure, ses minutes.
	 * Enfin l'utilisateur accede a un ensemble d'informations sur la vente de places.
	 */
	public static void vendrePlaceTheatre () {  
		//CHOIX SEMAINE
		
		ProgrammationSemaine l = null;
		System.out.println("Pour quelle semaine ? ");
		if(getLesProgrammations().size() != 0) {
			int choix = 0;
			boolean semaineExistante = true;
	        System.out.print("Saisir le numero de la semaine a modifier : ");
	        while(semaineExistante){
	        		try {
	        			choix = (int) Integer.valueOf(sc.next());
	        			
			        	if(choix>0 && choix<=getLesProgrammations().size()) {					 
			                System.out.println("Le numero de la semaine saisie est : " + choix);		
			                l = lesProgrammations.get(choix-1);
			                semaineExistante = false; 
			             
	                } else {
	        				System.out.println("Le numero de semaine saisi est inexistant.");
	        				System.out.print("Saisir le numero de la semaine a modifier : ");
	        			}
			      
	        		} catch(NumberFormatException e){
	        			System.out.print("Saisir le numero de la semaine a modifier : ");
	        		}
	        }
	       
		
			//CHOIX PIECE THEATRE
	        Set<PieceTheatre> x = l.recupTheatre();
	        if(!x.isEmpty()) {
		        System.out.println("Pour quelle piece de theatre ? \n");
				
				int nbPT =0;
				PieceTheatre pT = null;
				for(PieceTheatre a : x) {
					nbPT++;
					System.out.println("\n Piece de theatre n°"+nbPT+ " : " +a.toString());
				}
				nbPT=0;
				Scanner in = new Scanner(System.in);
				boolean choixTitre = true;
				int choix2 =0;
				while(choixTitre) {
					try {
						choix2 = (int) Integer.valueOf(in.next());
						if(choix2>0 && choix2<=x.size()) {	
						
							for(PieceTheatre y : x ) {
								choix2--;
								if (choix2 == 0)
									pT = y;
							}
							Set<SeanceTheatre> z = l.getEnsSeancesTheatre(pT);
							System.out.println(z);
							choixTitre=false;
							//in.close();
						}
					} catch(NumberFormatException e){
						for(	PieceTheatre a : x) {
							nbPT++;
							System.out.println("\n Piece de theatre n°"+nbPT+ " : " +a.toString());
						}
						nbPT=0;
						System.out.println("Pour quelle piece de theatre ? \n");
	        			}
				}
				
				
				
				// CHOIX SEANCE THEATRE
				System.out.println("Saisir le jour de la seance : " );
				boolean choixJour = true;
				int jour2=0;
				while(choixJour) {
					try {
						jour2 = (int) Integer.valueOf(in.next());
						
						if(l.getEnsSeancesTheatre(pT, jour2).size() > 0) { 																		
							choixJour = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir le jour de la seance : " );
	        			}
				}
				
				// CHOIX HEURE SEANCE THEATRE
				System.out.println("Saisir l'heure de la seance : ");
				boolean choixHeure = true;
				int heure2=0;
				while(choixHeure) {
					try {
						heure2 = (int) Integer.valueOf(in.next());
						if(heure2 >= 0 && heure2 <=24) {
							choixHeure = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir l'heure de la seance : ");
					}
	    			}
				// CHOIX MIN SEANCE THEATRE
				System.out.println("Saisir les minutes : ");
				//int minute2 = Integer.parseInt(JOptionPane.showInputDialog("Minute ? "));
				boolean choixMinute = true;
				int minute2=0;
				while(choixMinute) {
					try {
						minute2 = (int) Integer.valueOf(in.next());
						if(minute2>=0 && minute2 <=59) {
							choixMinute = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir les minutes : ");
					}
				}
				
				
				Heure Horaire =  new Heure(heure2 ,minute2);
				SeanceTheatre sT = l.consulterSeance(pT,jour2 , Horaire);
				System.out.println("Nb de Fauteuil disponible : " + sT.nbFauteuilsDispo());
				System.out.println("Nb de Fauteuil vendu : " + sT.getNbFauteuilsVendus());
		
				System.out.println("Nb de places normale disponible : " + sT.nbPlacesDispo());
				System.out.println("Nb de places normale vendu : " + sT.getNbPlacesVenduesTN());
				System.out.println("Nb de places total vendu pour cette seance  : " + sT.totalVendu());
		
				System.out.println("Combien de tickets de fauteuil normal ? ");
			
				// CHOIX TARIF FN
				boolean choixFN = true;
				int a=0;
				while(choixFN) {
					try {
						a = (int) Integer.valueOf(in.next());
						if(a>0 && a <=sT.nbFauteuilsDispo()) {
							sT.vendrePlacesFauteuil(a);
							choixFN = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Combien de tickets de fauteuil normal ? ");
					}
				}
				
				// CHOIX TARIF PN
				System.out.println("Combien de tickets de place normal ? ");
				boolean choixPN = true;
				int b=0;
				while(choixPN) {
					try {
						b = (int) Integer.valueOf(in.next());
						if(b>0 && b <=sT.nbPlacesDispo()) {
							sT.vendrePlacesTN(b);
							choixPN = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Combien de tickets de place normal ? ");
					}
				}
			
				double prix = a * (sT.getSalle().getPrixFauteuil())     + b*(sT.getSalle().getTarif()) ;
				System.out.println("A regler : "+prix +" €");
				ChiffreAffaire += prix;
			} else {
				System.out.println("Aucune programmation existante");
			}
	    } else {
	    		System.out.println("Aucune piece de theatre existante pour cette semaine");
	    }

	}
	
	/*
	 * Methode getChiffreAffaire
	 * 
	 * Cette methode retourne le chiffre d'affaire, attribut static de cette classe.
	 * @ChiffreAffaire : Double
	 */
	public static double getChiffreAffaire() {
		return ChiffreAffaire;
	}

	/*
	 * Methode creationSeanceTheatre
	 * 
	 * Cette methode propose de maniere interactive a l'utilisateur de saisir les champs d'une seance de theatre en creant un objet SeanceTheatre.
	 * Cet objet est ensuite ajouter a un Set de SeanceTheatre qui est retourne.
	 */
	public static Set<SeanceTheatre> creationSeanceTheatre() {	

		Set<SeanceTheatre> resu = new HashSet<SeanceTheatre>();

		boolean programme2 = true;
			String choix2="";
		while(programme2) {
			System.out.println("1. Programmer une nouvelle seance de piece de theatre ");
			System.out.println("2. Fin de la programmation cette piece de theatre \n");

			choix2 = sc.next();
			Scanner sc1 = new Scanner(choix2);
			while(!sc1.hasNextInt()) {
				choix2 = sc1.next();
				sc1 = new Scanner(choix2);
			}
			sc1.close();

			
			
			switch (choix2) {
			case "1":
				int nbSalleT = 0;
				for(SalleTheatre x : EnsembleSallesTheatre) {
					nbSalleT++;
					System.out.println("\n Salle n°"+nbSalleT+" : " + x.toString());
				}
				nbSalleT = 0;
				System.out.println();
				System.out.println("Entrez le numero de la salle :");
				boolean choixSalle = true;
				int salle;
				SalleTheatre salleT = null;
				Scanner in = new Scanner(System.in);
				while(choixSalle) {
					try {
						salle = (int) Integer.valueOf(in.next());
						if(salle>0 && salle<=EnsembleSallesTheatre.size()) {	
							salleT = EnsembleSallesTheatre.get(salle-1);
							choixSalle = false;
						}
					} catch(NumberFormatException e){
						for(SalleTheatre x : EnsembleSallesTheatre) {
							nbSalleT++;
							System.out.println("\n Salle n°"+nbSalleT+" : " + x.toString());
						}
						nbSalleT = 0;
	        			}
				}
				
		
				System.out.println("Saisir le jour de la seance : " );
				boolean choixJour = true;
				int jour2=0;
				while(choixJour) {
					try {
						jour2 = (int) Integer.valueOf(in.next());
						if(jour2 == 1 ||jour2 == 2 ||jour2 == 3 ||jour2 == 4 ||jour2 == 5 ||jour2 == 6 ||jour2 == 7) {
							choixJour = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir le jour de la seance : " );
	        			}
				}
				
				//int heure2 = Integer.parseInt(JOptionPane.showInputDialog("heure ? "));
				System.out.println("Saisir l'heure de la seance : ");
				boolean choixHeure = true;
				int heure2=0;
				while(choixHeure) {
					try {
						heure2 = (int) Integer.valueOf(in.next());
						if(heure2 >= 0 && heure2 <=24) {
							choixHeure = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir l'heure de la seance : ");
					}
        			}
				
				System.out.println("Saisir les minutes : ");
				//int minute2 = Integer.parseInt(JOptionPane.showInputDialog("Minute ? "));
				boolean choixMinute = true;
				int minute2=0;
				while(choixMinute) {
					try {
						minute2 = (int) Integer.valueOf(in.next());
						if(minute2>=0 && minute2 <=59) {
							choixMinute = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir les minutes : ");
					}
				}
				resu.add(new SeanceTheatre(salleT , jour2 , new Heure(heure2 , minute2 )));
				break;

			case "2":
				programme2=false;
				//sc.close();
				break;

			}

		}
		return resu;
	}
	
	/*
	 * Methode creationSeanceCinema
	 * 
	 * Cette methode propose de maniere interactive a l'utilisateur de saisir les champs d'une seance de cinema en creant un objet SeanceCinema.
	 * Cet objet est ensuite ajouter a un Set de SeanceCinema qui est retourne.
	 */
	public static Set<SeanceCinema> creationSeanceCinema() {		

		Set<SeanceCinema> resu = new HashSet<SeanceCinema>();
		
		boolean programme = true;
		String choix2="";
		while(programme) {
			System.out.println("1. Programmer une nouvelle seance pour ce film ");
			System.out.println("2. Fin de la programmation de ce film \n");
			choix2 = sc.next();
			Scanner sc1 = new Scanner(choix2);
			while(!sc1.hasNextInt()) {
				choix2 = sc1.next();
				sc1 = new Scanner(choix2);
			}
			sc1.close();
			

			
			switch (choix2) {
			case "1":
				int nbSalle = 0;
				for(Salle x : EnsembleSallesCinema) {
					nbSalle++;
					System.out.println("\n Salle n°"+nbSalle+" : " + x.toString());
				}
				nbSalle=0;
				System.out.println();
				System.out.println("Saisir le numero de la salle : ");
				boolean choixSalle = true;
				Salle ST = null;
				int salleC=0;
				Scanner in = new Scanner(System.in);
				while(choixSalle) {
					try {
						salleC = (int) Integer.valueOf(in.next());
						if(salleC>0 && salleC<=EnsembleSallesCinema.size()) {	
							ST = EnsembleSallesCinema.get(salleC-1);
							choixSalle = false;
						}
					} catch(NumberFormatException e){
					for(Salle x : EnsembleSallesCinema) {
						nbSalle++;
						System.out.println("\n Salle n°"+nbSalle+" : " + x.toString());
					}
					}
					nbSalle=0;
        			}
			
				System.out.println("Saisir le jour de la seance : " );
				boolean choixJour = true;
				int jour=0;
				while(choixJour) {
					try {
						jour = (int) Integer.valueOf(in.next());
						if(jour == 1 ||jour == 2 ||jour == 3 ||jour == 4 ||jour == 5 ||jour == 6 ||jour == 7) {
							choixJour = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir le jour de la seance : " );
	        			}
				}
				System.out.println("Saisir l'heure de la seance : ");
				boolean choixHeure = true;
				int heure=0;
				while(choixHeure) {
					try {
						heure = (int) Integer.valueOf(in.next());
						if(heure >= 0 && heure <=24) {
							choixHeure = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir l'heure de la seance : ");
					}
        			}
				
				System.out.println("Saisir les minutes : ");
				//int minute2 = Integer.parseInt(JOptionPane.showInputDialog("Minute ? "));
				boolean choixMinute = true;
				int minute=0;
				while(choixMinute) {
					try {
						minute = (int) Integer.valueOf(in.next());
						if(minute>=0 && minute <=59) {
							choixMinute = false;
						}
					}catch(NumberFormatException e){
						System.out.println("Saisir les minutes : ");
					}
				}
				resu.add(new SeanceCinema(ST ,jour , new Heure(heure , minute)));
				
				break;
			case "2":
				programme=false;
				//sc.close();
				break;

			}
		}
		return resu ;
	}

	/*
	 * Methode creationFilm
	 * 
	 * @titre : String
	 * @interprete : String
	 * @realisateur : String
	 * @heure : int
	 * @minute : int
	 * @duree : Heure
	 * @f : Film
	 * 
	 * Cette methode demande de maniere interactive a l'utilisateur de creer un film et le retourne
	 * 
	 */
	public static Film creationFilm() {		
		System.out.println("Saisir le titre du film : ");
		String titre = sc.next();
		System.out.println("Saisir le nom de l'interprete : ");
		String interprete = sc.next();
		System.out.println("Saisir le nom du realisateur : ");
		String realisateur = sc.next();
		
		System.out.println("Saisir la duree du film : ");
		System.out.println("Saisir le nombre d'heures : ");
		
		boolean choixHeure = true;
		int heure=0;
		while(choixHeure) {
			try {
				heure = (int) Integer.valueOf(sc.next());
				if(heure >= 0 && heure <=24) {
					choixHeure = false;
				}
			}catch(NumberFormatException e){
				System.out.println("Saisir le nombre d'heures : ");
			}
		}
	
		System.out.println("Saisir les minutes : ");
		//int minute2 = Integer.parseInt(JOptionPane.showInputDialog("Minute ? "));
		boolean choixMinute = true;
		int minute=0;
		while(choixMinute) {
			try {
				minute = (int) Integer.valueOf(sc.next());
				if(minute>=0 && minute <=59) {
					choixMinute = false;
				}
			}catch(NumberFormatException e){
				System.out.println("Saisir les minutes : ");
			}
		}
		System.out.println("Saisir le nombre de minutes : ");
		
		
		Heure duree = new Heure(heure, minute);
		Film f = new Film(titre,interprete,realisateur, duree);
		return f;

	}
	
	/*
	 * Methode creationTheatre 
	 * 
	 * @titre : String
	 * @interprete : String
	 * @metteurEnScene : int
	 * @pT : PieceTheatre
	 * 
	 * Cette methode demande de maniere interactive a l'utilisateur de creer une piece de theatre et la retourne
	 * 
	 */
	public static PieceTheatre creationTheatre() {	
				System.out.println("Saisir le titre de la piece  : ");
				String titre = sc.next();
				System.out.println("Saisir le nom de l'interprete : ");
				String interprete = sc.next();
				System.out.println("Saisir le nom du realisateur : ");
				String metteurEnScene = sc.next();
				
				System.out.println("Saisir le nombre d'entractes : ");
				boolean nbEntr = true;
				int nbEntractes=0;
				while(nbEntr) {
					try {
						nbEntractes = (int) Integer.valueOf(sc.next());
						nbEntr = false;
						
					}catch(NumberFormatException e){
						System.out.println("Saisir le nombre d'entractes : ");
					}
				}
				PieceTheatre pT = new PieceTheatre(titre,interprete,metteurEnScene, nbEntractes);
				return pT;
	}

	/*
	 * Methode getLesProgrammations
	 * 
	 * Cette methode get la liste lesProgrammations
	 */
	public static ArrayList<ProgrammationSemaine> getLesProgrammations() {
		return lesProgrammations;
	}
	
	/*
	 * Methode afficherLesProgrammations
	 * 
	 * Cette methode affiche l'ensemble des objets de type ProgrammationSemaine de la liste lesProgrammations
	 */
	public static void afficherLesProgrammes() {
		for (int i = 0; i < lesProgrammations.size(); i++) {
			System.out.println(lesProgrammations.get(i).toString());
		}

	}

	/*
	 * Methode lireFichierSallesCinema
	 * 
	 * Cette methode lit le contenu du fichier .csv "SallesCinema" place en parametre. 
	 * Extrait les informations sur les salles et les utilisent comme champs pour instancier une salle qui ensuite ajoutee a une liste de salle EnsembleSallesCinema.
	 */
	public static void lireFichierSallesCinema(String sallesCinemas) throws IOException { 

		String csvFile = sallesCinemas;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ";";
		EnsembleSallesCinema = new ArrayList<Salle>();

		br = new BufferedReader(new FileReader(csvFile));
		br.readLine();
		while ((line = br.readLine()) != null) {

			String[] Elements = line.split(cvsSplitBy);
			EnsembleSallesCinema.add(new Salle(Elements[0], Integer.parseInt(Elements[1]), Double.parseDouble(Elements[2])));

		}
		br.close();

	}

	/*
	 * Methode lireFichierSallesTheatres
	 * 
	 * Cette methode lit le contenu du fichier .csv "SallesTheatres" place en parametre. 
	 * Extrait les informations sur les salles et les utilisent comme champs pour instancier une salle qui ensuite ajoutee a une liste de salle EnsembleSallesTheatre.
	 */
	public static void lireFichierSallesTheatres(String sallesTheatres) throws IOException { 
		String csvFile = sallesTheatres;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ";";
		EnsembleSallesTheatre = new ArrayList<SalleTheatre>();

		br = new BufferedReader(new FileReader(csvFile));
		br.readLine();
		while ((line = br.readLine()) != null) {

			String[] Elements = line.split(cvsSplitBy);
			EnsembleSallesTheatre.add(new SalleTheatre(Elements[0], Integer.parseInt(Elements[1]),
					Double.parseDouble(Elements[2]), Integer.parseInt(Elements[3]), Double.parseDouble(Elements[4])));
		}
		br.close();


	}
}

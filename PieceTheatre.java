package TheProject2017;

public class PieceTheatre extends Spectacle {
	String metteurEnScene;
	int nbEntractes;

	public PieceTheatre(String titre, String interpretes, String metteurEnScene, int nbEntractes) {
		super(titre, interpretes);
		this.metteurEnScene = metteurEnScene;
		this.nbEntractes = nbEntractes;
	}

	public String getMetteurEnScene() {
		return this.metteurEnScene;
	}

	public int getNbEntractes() {
		return this.nbEntractes;
	}

	// @Override
	public String toString() {
		return super.toString() + "Metteur en scene : " + this.metteurEnScene + "\n Nombre d'entractes : "
				+ this.nbEntractes;
	}
}
